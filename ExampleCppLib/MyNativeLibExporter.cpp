#include "MyNativeLib.h"

#ifdef __cplusplus
extern "C" {
#endif
#ifdef _WIN32
#define MODULE_API __declspec(dllexport)
#else
#define MODULE_API
#endif
	MODULE_API int MyNativeLib_getNumber() {
		return MyNativeLib::getNumber();
	}
#ifdef __cplusplus
}
#endif
