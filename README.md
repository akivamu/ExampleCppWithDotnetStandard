This is example of how to use Unmanaged C++ code in .NET Standard/Core project.

At the writing time, [C++/CLI isn't supported in .NET Core](https://stackoverflow.com/questions/39140014/c-cli-support-in-net-core).  
So that you can't use C++ code directly inside .NET project (like .NET Framework).  
We have to use [P/Invoke](https://docs.microsoft.com/en-us/dotnet/framework/interop/platform-invoke-examples).

Steps:

- Build C++ into DLL, that exports public entry points to be used outside.
- In .NET, use P/Invoke to load C++ DLL file, map DLL exported entry points into .NET methods.

# Usage of example

There are 3 projects:

- `ExampleCppLib`: contain C++ code, will be built into DLL.
- `ExampleDotnetStandardLib`: .NET Standard library as a wrapper of exported C++ DLL entry points.
- `ExampleDotnetStandardLibTest`: Test for .NET Standard lib.

## Project: ExampleCppLib

In C++ code, use `dllexport` to export any functions you want to use in .NET.

You can build this project by Visual Studio to get win-x64 DLL file (will be copied to `/native-lib/`)

There is also CI setup to build this project into linux so file, use docker image: `matrim/cmake-examples`  
If you have docker (linux container), you can also build at local:  
```
docker run --rm -v ${pwd}:/data/code -it matrim/cmake-examples:3.5.1 /bin/bash -c "cd /data/code/ && chmod 777 build.linux.sh && ./build.linux.sh"
```
Output: `libExampleCppLib.so`

## Project: ExampleDotnetStandardLib

This project is C# .NET Standard library, acted like a wrapper of C++ functions:
```
[DllImport("ExampleCppLib", EntryPoint = "any_exported_cpp_function")]
public static extern void ThisIsWrapperFunctionForAboveEntryPoint();
```
Here we use P/Invoke (`DllImport`) that wrap C++ function inside DLL (`any_exported_cpp_function`) into our C# function (`ThisIsWrapperFunctionForAboveEntryPoint`)

This project use built C++ library (located at `/native-lib/`), so you need to build project `ExampleCppLib` first.  
When packaging this project, all C++ library (dll and so) will be included in nuget package.

## Project: ExampleDotnetStandardLibTest

This project just reference to the wrapper lib `ExampleDotnetStandardLib`.  
So it doesn't need to aware about C++ underlying, just use C# library.
